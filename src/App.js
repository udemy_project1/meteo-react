import { useEffect, useState } from "react";
import "./App.css";
import loader from "./assets/loader.svg";
import browser from "./assets/browser.svg";

const WEATHER_URL = process.env.REACT_APP_BASE_WEATHER_URL;
const APIKEY = process.env.REACT_APP_API_WEATHER_KEY;
function App() {
	const [weatherData, setWeatherData] = useState(null);
	const [errorInfo, setErrorInfo] = useState(null);
	useEffect(() => {
		fetch(`${WEATHER_URL}/current.json?key=${APIKEY}&q=Paris`)
			.then((response) => {
				console.log(response);
				// if (!response.ok) throw new Error(`Error ${response.status} ${response.statusText}`);
				return response.json();
			})
			.then((responseData) => {
				if (responseData.error) throw new Error(`Error ${responseData.error.code} ${responseData.error.message}`);
				setWeatherData({
					country: responseData.location.country,
					city: responseData.location.name,
					temperature: responseData.current.temp_c,
					icon: responseData.current.condition.icon,
				});
			})
			.catch((err) => {
				console.log(err);
				console.dir(err);
				setErrorInfo(err.message);
			});
	}, []);
	return (
		<div className="bill__weather">
			<div className={`bill__weather-loader_container ${!weatherData && !errorInfo && "active"}`}>
				<img
					src={loader}
					alt="loading icon"
				/>
			</div>
			{weatherData && (
				<>
					<p className="bill__weather-city_name">{weatherData.city}</p>
					<p className="bill__weather-country_name">{weatherData.country}</p>
					<p className="bill__weather-temperature">{weatherData.temperature}°C</p>
					<div className="bill__weather-info_icon-container">
						<img
							src={weatherData.icon}
							alt="weather icon"
							className="bill__weather-info_icon-container_img"
						/>
					</div>
				</>
			)}
			{errorInfo && !weatherData && (
				<>
					<div className="bill__weather-error_container">
						<p className="bill_weather-error_container-info">{errorInfo}</p>
						<img
							src={browser}
							alt="error icon"
							className="bill__weather-erro_container-img"
						/>
					</div>
				</>
			)}
		</div>
	);
}

export default App;
